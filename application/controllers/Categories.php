<?php
require APPPATH . 'libraries/REST_Controller.php';
defined('BASEPATH') OR exit('No direct script access allowed');
    
class Categories extends REST_Controller {
       /**
        * Get All Data from this method.
        *
        * @return Response
       */
       public function __construct() {
        //load database in autoload libraries 
          parent::__construct(); 
		  $this->load->model('Category_model');      
       }
	   
    public function index_get()
       {
           $category_model = new Category_model;
           $data = $category_model->get_categories();
		   $response = ['status'=>true,'msg'=>'All categories','data'=>$data];
		   $code = REST_Controller::HTTP_OK ;
		   $this->logs($response,$code);
           return $this->response($response, $code);
       }

    public function categoryproduct_get($id)
        {
            $category_model = new Category_model;
            $data = $category_model->all_products($id);
            $response = ['status'=>true,'msg'=>'Category Product','data'=>$data];
            $code = REST_Controller::HTTP_OK ;
            $this->logs($response,$code);
            return $this->response($response, $code);
        }
	   
}
