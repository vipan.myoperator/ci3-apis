<?php
    
class LogViewerController extends CI_Controller {
       
		private $logViewer;

		public function __construct() {
			$this->logViewer = new \CILogViewer\CILogViewer();
		//...
		}

		public function index() {
			echo $this->logViewer->showLogs();
			return;
		}

		public function test() {
			log_message('error', 'Error');
			echo 'test ok';
			return;
		}		
	   
}
