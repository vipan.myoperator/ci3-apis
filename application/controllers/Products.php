<?php
require APPPATH . 'libraries/REST_Controller.php';
defined('BASEPATH') OR exit('No direct script access allowed');
    
class Products extends REST_Controller {
       /**
        * Get All Data from this method.
        *
        * @return Response
       */
       public function __construct() {
        //load database in autoload libraries 
          parent::__construct(); 
		  $this->load->model('Product_model');
		  $this->load->model('User_model');
		  $this->load->model('Category_model');      
       }
	   
    public function index_get()
       {
           $product_model = new Product_model;
           $data = $product_model->get_products();
		   $response = ['status'=>true,'msg'=>'All products','data'=>$data];
		   $code = REST_Controller::HTTP_OK ;
		   $this->logs($response,$code);
           return $this->response($response, $code);
	   }
	   
	   /**
        * Create new product.
        *
        * @return Response
       */
    public function create_post()
      {
		   $product_model = new Product_model;
		   $category_model = new Category_model;
           
           $this->load->library('form_validation');
           $this->form_validation->set_data($this->post());
           $this->form_validation->set_rules('name', 'Name', 'required');
		   $this->form_validation->set_rules('price', 'Price', 'required');
		   $this->form_validation->set_rules('categories_ids', 'Categories', 'callback_check_categories');
		   $this->form_validation->set_rules('user_id', 'User ID', 'required|callback_check_permissions[create]');
           
           if ($this->form_validation->run() == FALSE)
           {
               $response = ['status'=>false,'errors'=>$this->form_validation->error_array()];
               $code = REST_Controller::HTTP_BAD_REQUEST ;
               $this->logs($response,$code);
               return $this->response($response, $code);
           }
           else
           {
			   	   $categories_ids = $this->post('categories_ids');
                   $data = array(
                       'name' => $this->post('name'),
                       'price' => $this->post('price'),
                       'description' => $this->post('description'),
                       'created_at' => date('Y-m-d H:i:s'),
                       'updated_at' => date('Y-m-d H:i:s'),
                   );
                   
				   $insert_id = $product_model->save_product($data, $categories_ids);

				   for($i = 0; $i<count($categories_ids); $i++){
					$category_model->save_relation($categories_ids[$i],$insert_id);
				   }
				   
                   $response = ['status'=>true,'msg'=>'Product created successfully.'];
                   $code = REST_Controller::HTTP_CREATED ;
                   $this->logs($response,$code);
                   return $this->response($response, $code);
              
           }
	   }
	   
	   /**
        * Fetch single product.
        *
        * @return Response
       */
	public function prodect_get($id)
	  {
		   $product_model = new Product_model;
		   $product = $product_model->getproduct($id);
		   if(empty($product)){
			   $response = ['status' => false, 'msg' => 'Product not found'];
			   $code = REST_Controller::HTTP_NOT_FOUND ;
			   $this->logs($response,$code);
			   return $this->response($response, $code);	
		   }
		   $product_data = [
			   'id' => $product['id'],
			   'name' => $product['name'],
			   'price' => $product['price'],
			   'description' => $product['description'],
			   'created_at' => $product['created_at'],
		   ];
		   $response = ['status' => true, 'msg'=> 'Product Found', 'data' => $product_data];
		   $code = REST_Controller::HTTP_OK ;
		   $this->logs($response,$code);
		   return $this->response($response, $code);
	  }

	/**
        * Update product.
        *
        * @return Response
       */
	  public function update_put($id)
	  {
		   $product_model = new Product_model;
		   $this->load->library('form_validation');

		   $this->form_validation->set_data($this->put());
		   $this->form_validation->set_rules('name', 'Name', 'required');
		   $this->form_validation->set_rules('price', 'Price', 'required');
		   $this->form_validation->set_rules('description', 'Description', 'required');
		   $this->form_validation->set_rules('user_id', 'User ID', 'required|callback_check_permissions[update]');

		   if ($this->form_validation->run() === FALSE)
		   {
			   $response = ['status'=>false,'errors'=>$this->form_validation->error_array()];
			   $code = REST_Controller::HTTP_BAD_REQUEST ;
			   $this->logs($response,$code);
			   return $this->response($response, $code);
		   }
		   else
		   {
			   if($product_model->getproduct($id) == false){
				   $code = REST_Controller::HTTP_NOT_FOUND ;
				   $response = ['status'=>false,'msg'=>'Product not found'];
				   $this->logs($response,$code);
				   return $this->response($response, $code);	
			   }else{
				   $data = array(
					   'name' => $this->put('name'),
					   'price' => $this->put('price'),
					   'description' => $this->put('description'),
					   'updated_at' => date('Y-m-d H:i:s'),
				   );
				   $product_model->update_product($id,$data);
				   $response = ['status' => true, 'msg' => 'Product updated successfully.'];
				   $code = REST_Controller::HTTP_OK ;
				   $this->logs($response,$code);
				   return $this->response($response, $code);
			   }
		   }
	  }

	  /**
        * Delete Product.
        *
        * @return Response
       */
	  public function productdelete_delete($id)
	  {
		  $product_model = new Product_model;
		  if($product_model->getproduct($id) == false){
			  $response = ['status' => false, 'msg' => 'Product not found'];
			  $code = REST_Controller::HTTP_NOT_FOUND ;
			  $this->logs($response,$code);
			  return $this->response($response, $code);	
		  } else {
			  $product_model->delete_product($id);
			  $response = ['status' => true, 'msg' => 'Product delete successfully.'];
			  $code = REST_Controller::HTTP_OK ;
			  $this->logs($response,$code);
			  return $this->response($response, $code);
		  }
	  }

	  /**
        * Categories Validation.
        *
        * @return Response
       */

	  function check_categories() {
		$categories = $this->post('categories_ids');
		if (is_array($categories) == false || empty($categories)) {
			$this->form_validation->set_message('check_categories', 'The Category field is required.');
			return false;
			
		}

		$last_name = $this->input->post('lastname');

		$this->db->select('id');
		$this->db->from('categories');
		$this->db->where_in('id', $categories);
		$query = $this->db->get();
		$num = $query->num_rows();
		
		if ($num > 0 && $num == count($categories)) {
			return TRUE;
		} else {
			$this->form_validation->set_message('check_categories', 'The Category field is not valid');
			return FALSE;
		}
	}

	/**
        * Categories Validation.
        *
        * @return Response
       */

	  function check_permissions($user_id,$action) {
		$user_model = new User_model;
		$user = $user_model->getuser($user_id);

		if($user == false){
			$this->form_validation->set_message('check_permissions', 'User not found');
			return FALSE;
		}

		$this->db->select('permissions.id');
		$this->db->from('permissions');
		$this->db->where('permissions.module', 'products');
		$this->db->where('permissions.action', $action);
        $this->db->where('permission_role.role_id', $user['role_id']);
        $this->db->join('permission_role', 'permissions.id = permission_role.permission_id', 'inner');
        $query = $this->db->get();
		$num = $query->num_rows();
		
		if ($num > 0) {
			return TRUE;
		} else {
			$this->form_validation->set_message('check_permissions', 'Permission denied');
			return FALSE;
		}
	}
	   
}
