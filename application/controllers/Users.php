<?php
require APPPATH . 'libraries/REST_Controller.php';
defined('BASEPATH') OR exit('No direct script access allowed');
    
class Users extends REST_Controller {
       /**
        * Get All Data from this method.
        *
        * @return Response
       */
       public function __construct() {
        //load database in autoload libraries 
          parent::__construct(); 
          $this->load->model('User_model');         
          $this->load->model('Role_model');         
       }
	   
       public function index_get()
       {
           $users = new User_model;
           $data = $users->get_users();
		   $response = ['status'=>true,'msg'=>'All users','data'=>$data];
		   $code = REST_Controller::HTTP_OK ;
		   $this->logs($response,$code);
           return $this->response($response, $code);
       }
	   
	   public function login_post()
       {
			$users = new User_model;
			$this->load->library('form_validation');
			$this->form_validation->set_data($this->post());
			$this->form_validation->set_rules('email', 'Email', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');
			
			if ($this->form_validation->run() == FALSE) {
				$response = ['status'=>false,'errors'=>$this->form_validation->error_array()];
				$code = REST_Controller::HTTP_BAD_REQUEST ;
				$this->logs($response,$code);
				return $this->response($response, $code);
			} else {
				$data = array(
					'email' => $this->post('email'),
					'password' => $this->post('password')
				);
				$user = $users->get_user_by_email($data['email']);
				if (!empty($user) && $user['password'] == md5($data['password'])) {
					$user_data = [
						'id' => $user['id'],
						'name' => $user['name'],
						'role_id' => $user['role_id'],
						'email' => $user['email'],
					];
					
					$response = ['status'=>true,'msg'=>'login successfully.','data'=>$user_data];
					$code = REST_Controller::HTTP_OK ;
					$this->logs($response,$code);
					return $this->response($response, $code);	
				}
				
				$response = ['status'=>false, 'msg' => 'These credentials do not match our records.'];
				$code = REST_Controller::HTTP_NOT_FOUND ;
				$this->logs($response,$code);
				return $this->response($response, $code);
			}
       }
	   
       /**
        * Create new User.
        *
        * @return Response
       */
       public function create_post()
       {
			$users = new User_model;
			$role_model = new Role_model;
			
			$this->load->library('form_validation');
			$this->form_validation->set_data($this->post());
			$this->form_validation->set_rules('name', 'Name', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');
			$this->form_validation->set_rules('role_id', 'Role_id', 'required|integer');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
			
			if ($this->form_validation->run() == FALSE)
			{
				$response = ['status'=>false,'errors'=>$this->form_validation->error_array()];
				$code = REST_Controller::HTTP_BAD_REQUEST ;
				$this->logs($response,$code);
				return $this->response($response, $code);
			}
			else
			{
				if($role_model->role_exists($this->post('role_id')) == false){
					$response = ['status'=>false,'msg'=>'Role does not exist.'];
					$code = REST_Controller::HTTP_BAD_REQUEST ;
					$this->logs($response,$code);
					return $this->response($response, $code);
				}else{
					$data = array(
						'name' => $this->post('name'),
						'email' => $this->post('email'),
						'password' => md5($this->post('password')),
						'address' => $this->post('address'),
						'role_id' => $this->post('role_id'),
						'created_at' => date('Y-m-d H:i:s'),
						'updated_at' => date('Y-m-d H:i:s'),
					);
					
					$users->save_user($data);
					$response = ['status'=>true,'msg'=>'User created successfully.'];
					$code = REST_Controller::HTTP_CREATED ;
					$this->logs($response,$code);
					return $this->response($response, $code);
				}
			}
        }
	
       /**
        * Fetch single User.
        *
        * @return Response
       */
       public function user_get($id)
       {
			$user_model = new User_model;
			$user = $user_model->getuser($id);
			if(empty($user)){
				$response = ['status' => false, 'msg' => 'User not found'];
				$code = REST_Controller::HTTP_NOT_FOUND ;
				$this->logs($response,$code);
				return $this->response($response, $code);	
			}
			$user_data = [
				'id' => $user['id'],
				'name' => $user['name'],
				'role_id' => $user['role_id'],
				'email' => $user['email'],
				'address' => $user['address'],
				'status' => $user['status'],
				'created_at' => $user['created_at'],
			];
			$response = ['status' => true, 'msg'=> 'User Found', 'data' => $user_data];
			$code = REST_Controller::HTTP_OK ;
			$this->logs($response,$code);
			return $this->response($response, $code);
       }
	   
       /**
        * Update User.
        *
        * @return Response
       */
       public function update_put($id)
       {
            $users = new User_model;
			$this->load->library('form_validation');
			$this->form_validation->set_data($this->put());
			$this->form_validation->set_rules('name', 'Name', 'required');
			$this->form_validation->set_rules('address', 'Address', 'required');

			if ($this->form_validation->run() === FALSE)
			{
				$response = ['status'=>false,'errors'=> $this->form_validation->error_array()];
				$code = REST_Controller::HTTP_BAD_REQUEST ;
				$this->logs($response,$code);
				return $this->response($response, $code);
			}
			else
			{
				if($users->getuser($id) == false){
					$response = ['status'=>false,'msg'=>'User not found'];
					$code = REST_Controller::HTTP_NOT_FOUND ;
					$this->logs($response,$code);
					return $this->response($response, $code);	
				}else{
					$data = array(
						'name' => $this->put('name'),
						'address' => $this->put('address'),
						'updated_at' => date('Y-m-d H:i:s'),
					);
					$users->update_user($id,$data);
					$response = ['status' => true, 'msg' => 'User updated successfully.'];
					$code = REST_Controller::HTTP_OK ;
					$this->logs($response,$code);
					return $this->response($response, $code);
				}
			}
       }
	   
       /**
        * Delete User.
        *
        * @return Response
       */
       public function userdelete_delete($id)
       {
		   $users = new User_model;
		   if($users->getuser($id) == false){
			   $response = ['status' => false, 'msg' => 'User not found'];
			   $code = REST_Controller::HTTP_NOT_FOUND ;
			   $this->logs($response,$code);
			   return $this->response($response, $code);	
		   } else {
			   $users->delete_user($id);
			   $response = ['status' => true, 'msg' => 'User delete successfully.'];
			   $code = REST_Controller::HTTP_OK ;
			   $this->logs($response,$code);
			   return $this->response($response, $code);
		   }
       }
	   
	   public function roles_get()
       {
           $role_model = new Role_model;
           $data= $role_model->get_roles();
		   $response = ['status'=>true,'msg'=>'roles','data'=>$data];
		   $code = REST_Controller::HTTP_OK ;
		   $this->logs($response,$code);
           return $this->response($response,$code);
       }
		
		
		
	   
	   
}
