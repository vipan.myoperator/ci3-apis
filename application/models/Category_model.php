<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Category_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function getcategory($id)
    {
        $query = $this->db->get_where('products', ['id' => $id, 'status' => 1]);
		$data = $query->result_array();
		if($data){
			return $data[0];
		}else{
			return false;
		}
    }
	
    public function get_categories(){
        $this->db->select('id, name, created_at');
        $query = $this->db->get_where('categories',['status' => 1]);
        return $query->result();
    }
    
    public function save_relation($id,$product_id)
    {
        $this->db->where('category_id', $id);
        $this->db->where('product_id', $product_id);
        $q = $this->db->get('product_relation');
        $this->db->reset_query();

        if ( $q->num_rows() == 0 ) 
        {
            $data = ["category_id"=>$id,"product_id"=>$product_id];
            return $this->db->insert('product_relation', $data);
        }else{
            return true ;
        }
        
    }

    public function all_products($id)
    {
        $this->db->select('products.id, products.name, products.price, products.description');
        $this->db->from('products');
        $this->db->where('categories.id', $id);
        $this->db->where('products.status', 1);
        $this->db->join('product_relation', 'products.id = product_relation.product_id', 'inner');
        $this->db->join('categories', 'product_relation.category_id = categories.id', 'inner');
        $query = $this->db->get();
        return $query->result();
    }
	
	

}
