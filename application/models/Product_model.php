<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Product_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function getproduct($id)
    {
        $query = $this->db->get_where('products', ['id' => $id, 'status' => 1]);
		$data = $query->result_array();
		if($data){
			return $data[0];
		}else{
			return false;
		}
    }
	
    public function get_products(){
        $this->db->select('name, price, description, created_at');
        $query = $this->db->get_where('products',['status' => 1]);
        return $query->result();
    }
    
    public function save_product($data, $categories_ids)
    {
        $this->db->insert('products', $data);
        return $this->db->insert_id();
    
    }

    public function update_product($id, $data)
    {
		$this->db->where('id', $id);
		return $this->db->update('products', $data);
    }

    public function delete_product($id)
    {
		$this->db->where('id', $id);
        $this->db->update('products', ['status' => 0]);
    }
	
	

}
