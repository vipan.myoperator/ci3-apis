<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Role_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }
	   
	public function role_exists($id){
		$this->db->where('role_id',$id);
		$query = $this->db->get('roles');
		if ($query->num_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function get_roles(){
		$query = $this->db->get("roles");
        return $query->result();
	}

}
