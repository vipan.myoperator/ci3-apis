<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class User_model extends CI_Model
{
    public function __construct()
    {
		$this->load->database();
    }
	
	public function getuser($id)
    {
        $query = $this->db->get_where('users', ['id' => $id, 'status' => 1]);
		$data = $query->result_array();
		if($data){
			return $data[0];
		}else{
			return false;
		}
    }
	
	public function get_user_by_email($email)
    {
        $query = $this->db->get_where('users', ['email' => $email, 'status' => 1]);
		$data = $query->result_array();
		if($data){
			return $data[0];
		}else{
			return false;
		}
    }
	

    public function get_users(){
		$this->db->select('id, name, email, address, role_id, created_at');
		$query = $this->db->get_where('users',['status' => 1]);
        return $query->result();
    }

    public function save_user($data)
    {
        return $this->db->insert('users', $data);
    }
	
	public function update_user($id, $data)
    {
		$this->db->where('id', $id);
		if (isset($data['password']) && !empty($data['password'])) {
			$data['password'] = md5($data['password']);
		}
		return $this->db->update('users', $data);
    }
	
	public function delete_user($id)
    {
		$this->db->where('id', $id);
        $this->db->update('users', ['status' => 0]);
    }
	
	public function is_user_exists($email) {
	   $this->db->where('email', $data['email']);
	   $query = $this->db->get('users');
	   if( $query->num_rows() == 1 )  {
		  return (array) $query->row();
       }else{
			return false;   
		}
    }
	   
	public function role_exists($key){
		$this->db->where('role_id',$key);
		$query = $this->db->get('roles');
		if ($query->num_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function get_roles(){
		$query = $this->db->get('roles');
        return $query->result();
	}

}
