<?php

class Apistest extends CIPHPUnitTestDbTestCase
{
	private $ci;
	public $userid;
	/**
	 * Set $strictRequestErrorCheck false, because the following error occurs if true
	 *
	 * 1) Example_test::test_users_get_format_csv
	 * RuntimeException: Array to string conversion on line 373 in file .../application/libraries/Format.php
	 *
	 * @var bool
	 */
	protected $strictRequestErrorCheck = false;

	/**
	 * Users, Users by ID, User 404 
	 */
	protected function setUp(): void
	{
		$this->loadDependencies();
		$roles = array(["role_id"=> "1","role_name"=> "Admin"],["role_id"=> "2","role_name"=> "Manager"],["role_id"=> "3",
			"role_name"=> "User"
		]);
		
		for($i=0; $i < count($roles); $i++){
			$this->hasInDatabaseID('role_id','roles',$roles[$i]);
		}

		$data = [
			"name"	=> "test",
			"password" => md5("123456"),
			"email" => "test@test.co",
			"address" => "address",
			"role_id" => 1
		];
		$dat2 = [
			"name"=> "test",
			"password"=> md5("123456"),
			"email"=> "testpost@test.co",
			"address"=> "address",
			"role_id"=> 1 
		];
		$table = "users";
		$this->insertCache[] = [
			$table, $dat2
		];

		$this->userid = $this->hasInDatabase($table,$data);
		$this->insertCache = array_reverse($this->insertCache);

	}

	
	public function test_users_get()
	{

		try {
			$output = $this->request('GET', '/');
		} catch (CIPHPUnitTestExitException $e) {
			$output = ob_get_clean();
		}
		
		$this->db->select('id,name,email,address,role_id,created_at');
		$this->db->where(['status'=>1]);
		$dbdata = $this->db->get('users')->result();

		$match = [
				"status"=> true,
				"msg"=> "All users",
				"data"=> $dbdata
		];

		$this->assertEquals(
			json_encode($match),
			$output
		);
		$this->assertResponseCode(200);
	}

	public function test_users_getwith_id()
	{
		$userid = (int)$this->userid;
		try {
			$output = $this->request('GET', "/userGet/{$userid}");
		} catch (CIPHPUnitTestExitException $e) {
			$output = ob_get_clean();
		}
		$this->db->select('id, name, role_id, email, address, status, created_at');
		$this->db->where(['id' => $userid]);
		$this->db->where(['status' => 1]);
		$dbdata = $this->db->get('users')->row();
		$match = [
				"status"=> true,
				"msg"=> "User Found",
				"data"=> $dbdata
		];
		$this->assertEquals(
			json_encode($match),
			$output
		);

		$this->assertResponseCode(200);
	}

	public function test_users_get_id_user_not_found()
	{
		$output = $this->request('GET', 'userGet/999');
		$this->assertEquals(
			'{"status":false,"msg":"User not found"}',
			$output
		);
		$this->assertResponseCode(404);
	}

	/**
	 * Create Register
	*/ 

	public function test_users_register()
	{

		$this->request->setHeader('Content-Type', 'application/json');
		$data = [
			"name"=> "test",
			"password"=> "123456",
			"email"=> "testpost@test.co",
			"address"=> "address",
			"role_id"=> 1 
		];
		
		$output = $this->request(
			'POST', 'register',
			 $data
		);
		$this->assertEquals('{"status":true,"msg":"User created successfully."}', $output);
	}

	public function test_users_register_role404()
	{
		$this->request->setHeader('Content-Type', 'application/json');
		$data = [
			"name"=> "test",
			"password"=> "123456",
			"email"=> "testrole@test.co",
			"address"=> "address",
			"role_id"=> 999
		];
		$output = $this->request(
			'POST', 'register',
			 $data
		);
		$this->assertEquals('{"status":false,"msg":"Role does not exist."}', $output);
	}

	public function test_users_register_validate()
	{
		$this->request->setHeader('Content-Type', 'application/json');
		$data = [
			"password"=> "123456",
			"email"=> "testValidate@test.co",
			"address"=> "address",
			"role_id"=> 1
		];
		$output = $this->request(
			'POST', 'register',
			 $data
		);
		$this->assertEquals('{"status":false,"errors":{"name":"The Name field is required."}}', $output);
	}

	/**
	 * Update User
	*/ 

	public function test_users_update()
	{
		$userid = (int)$this->userid;
		$this->request->setHeader('Content-Type', 'application/json');
		$data = [
			"name"=> "test",
			"address"=> "address" 
		];
		$output = $this->request(
			'PUT', "usersUpdate/{$userid}",
			 json_encode($data)
		);

		$this->assertEquals('{"status":true,"msg":"User updated successfully."}', $output);
	}

	public function test_users_update_404()
	{
		$this->request->setHeader('Content-Type', 'application/json');
		$data = [
			"name"=> "test",
			"address"=> "address" 
		];
		$output = $this->request(
			'PUT', 'usersUpdate/999',
			 json_encode($data)
		);

		$this->assertEquals('{"status":false,"msg":"User not found"}', $output);
	}

	public function test_users_update_validate()
	{
		$userid = (int)$this->userid;
		$this->request->setHeader('Content-Type', 'application/json');
		$data = [
			"address"=> "address" 
		];
		$output = $this->request(
			'PUT', "usersUpdate/{$userid}",
			 json_encode($data)
		);

		$this->assertEquals('{"status":false,"errors":{"name":"The Name field is required."}}', $output);
	}

	/**
	 * Delete User
	*/ 

	public function test_users_delete()
	{
		$userid = (int)$this->userid;
		$this->request->setHeader('Accept', 'application/json');
		$output = $this->request(
			'DELETE', "usersDelete/{$userid}"
		);
		$this->assertEquals('{"status":true,"msg":"User delete successfully."}', $output);
	}

	public function test_users_delete_404()
	{
		$this->request->setHeader('Accept', 'application/json');
		$output = $this->request(
			'DELETE', 'usersDelete/999'
		);
		$this->assertEquals('{"status":false,"msg":"User not found"}', $output);
	}

	/**
	 * User login
	*/ 

	public function test_users_login()
	{
		$userid = (int)$this->userid;

		$this->request->setHeader('Accept', 'application/json');
		$data = [
			"email"=> "test@test.co", 
			"password"=> "123456",
		];
		$output = $this->request(
			'POST', 'login',
			 $data
		);
		$data['password'] = md5($data['password']);
		$dbdata = $this->grabFromDatabaserow('users', 'id, name, role_id, email', $data);
		
		$match = [
				"status"=> true,
				"msg"=> "login successfully.",
				"data"=> $dbdata
		];


		$this->assertEquals(json_encode($match), $output);
	}

	public function test_users_login_validate()
	{
		$this->request->setHeader('Accept', 'application/json');
		$data = [
			"password"=> "123456",
		];
		$output = $this->request(
			'POST', 'login',
			 $data
		);
		unset($data['password']);
		$response = [
			"status"=> false,
			"errors"=> [
				"email"=> "The Email field is required."
			]
			];

		$this->assertEquals(json_encode($response), $output);
	}

	public function test_users_login_404()
	{
		$this->request->setHeader('Accept', 'application/json');
		$data = [
			"email"=> "test@error.co", 
			"password"=> "123456",
		];
		$output = $this->request(
			'POST', 'login',
			 $data
		);
		unset($data['password']);
		$response = [
			"status"=> false,
			"msg"=> "These credentials do not match our records."
			];

		$this->assertEquals(json_encode($response), $output);
	}

	/**
	 * User Roles
	*/ 

	public function test_users_roles()
	{
		$output = $this->request(
			'get', 'rolesGet'
		);
		$this->db->select('role_id, role_name');
		$dbdata = $this->db->get('roles')->result();
		$response = [
			"status"=> true,
			"msg"=> "roles",
			"data"=> $dbdata
		];

		$this->assertEquals(json_encode($response), $output);
	}
	 
}