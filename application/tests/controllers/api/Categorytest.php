<?php

class Categorytest extends CIPHPUnitTestDbTestCase
{
	private $ci;
	public $userid;
	/**
	 * Set $strictRequestErrorCheck false, because the following error occurs if true
	 *
	 * 1) Example_test::test_users_get_format_csv
	 * RuntimeException: Array to string conversion on line 373 in file .../application/libraries/Format.php
	 *
	 * @var bool
	 */
	protected $strictRequestErrorCheck = false;

	/**
	 * Users, Users by ID, User 404 
	 */
	protected function setUp(): void
	{
		$this->loadDependencies();
		$category = array(
			["id"=> "1","name"=> "Men Wear"],
			["id"=> "2","name"=> "Girl Wear"],
			["id"=> "3","name"=> "Jeans"],
			["id"=> "4","name"=> "T-shirt"],
			["id"=> "5","name"=> "Shoes"],
			["id"=> "6","name"=> "Nike"],
			["id"=> "7","name"=> "Adidas"]				
		);
		
		for($i=0; $i < count($category); $i++){
			$this->hasInDatabaseID('id','categories',$category[$i]);
		}

	}

	public function test_categories_get()
	{

		try {
			$output = $this->request('GET', '/categories');
		} catch (CIPHPUnitTestExitException $e) {
			$output = ob_get_clean();
		}
		
		$this->db->select('id,name,created_at');
		$dbdata = $this->db->get('categories')->result();

		$match = [
				"status"=> true,
				"msg"=> "All categories",
				"data"=> $dbdata
		];

		$this->assertEquals(
			json_encode($match),
			$output
		);
		$this->assertResponseCode(200);
	}

	 
}