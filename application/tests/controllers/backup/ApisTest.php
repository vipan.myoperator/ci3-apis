<?php

class ApisTest extends TestCase
{
	private $ci;
	/**
	 * Set $strictRequestErrorCheck false, because the following error occurs if true
	 *
	 * 1) Example_test::test_users_get_format_csv
	 * RuntimeException: Array to string conversion on line 373 in file .../application/libraries/Format.php
	 *
	 * @var bool
	 */
	protected $strictRequestErrorCheck = false;

	/**
	 * Users, Users by ID, User 404 
	 */
	protected function setUp(): void
	{
			$CI =& get_instance();
			$CI->load->database();
			$this->db = $CI->db;
	}

	
	public function test_users_get()
	{

		try {
			$output = $this->request('GET', '/');
		} catch (CIPHPUnitTestExitException $e) {
			$output = ob_get_clean();
		}

		$this->assertEquals(
			'{"status":true,"msg":"All users","data":[{"id":"1","name":"vipan","email":"vipan.kumar1@myoperator.co","address":"mohali","role_id":"1","created_at":"2021-01-11 18:02:42"}]}',
			$output
		);
		$this->assertResponseCode(200);
	}
 
	 public function test_users_getwith_id()
	{
		try {
			$output = $this->request('GET', '/userGet/1');
		} catch (CIPHPUnitTestExitException $e) {
			$output = ob_get_clean();
		}

		$this->assertEquals(
		'{"status":true,"msg":"User Found","data":{"id":"1","name":"vipan","role_id":"1","email":"vipan.kumar1@myoperator.co","address":"mohali","status":"1","created_at":"2021-01-11 18:02:42"}}',
			$output
		);
		$this->assertResponseCode(200);
	}

	public function test_users_get_id_user_not_found()
	{
		$output = $this->request('GET', 'userGet/999');
		$this->assertEquals(
			'{"status":false,"msg":"User not found"}',
			$output
		);
		$this->assertResponseCode(404);
	}

	/**
	 * Create Register
	*/ 

	public function test_users_register()
	{
		$this->request->setHeader('Content-Type', 'application/json');
		$data = [
			"name"=> "test",
			"password"=> "123456",
			"email"=> "test@test.co",
			"address"=> "address",
			"role_id"=> 1 
		];
		$output = $this->request(
			'POST', 'register',
			 $data
		);
		unset($data['password']);
		$this->tearDowns($data,'users');

		$this->assertEquals('{"status":true,"msg":"User created successfully."}', $output);
	}

	public function test_users_register_role404()
	{
		$this->request->setHeader('Content-Type', 'application/json');
		$data = [
			"name"=> "test",
			"password"=> "123456",
			"email"=> "test@test.co",
			"address"=> "address",
			"role_id"=> 999
		];
		$output = $this->request(
			'POST', 'register',
			 $data
		);
		$this->assertEquals('{"status":false,"msg":"Role does not exist."}', $output);
	}

	public function test_users_register_validate()
	{
		$this->request->setHeader('Content-Type', 'application/json');
		$data = [
			"password"=> "123456",
			"email"=> "test@test.co",
			"address"=> "address",
			"role_id"=> 1
		];
		$output = $this->request(
			'POST', 'register',
			 $data
		);

		$this->assertEquals('{"status":false,"errors":{"name":"The Name field is required."}}', $output);
	}

	/**
	 * Update User
	*/ 

	public function test_users_update()
	{
		$this->request->setHeader('Content-Type', 'application/json');
		$data = [
			"name"=> "vipan",
			"address"=> "mohali" 
		];
		$output = $this->request(
			'PUT', 'usersUpdate/1',
			 json_encode($data)
		);

		$this->assertEquals('{"status":true,"msg":"User updated successfully."}', $output);
	}

	public function test_users_update_404()
	{
		$this->request->setHeader('Content-Type', 'application/json');
		$data = [
			"name"=> "vipan",
			"address"=> "mohali" 
		];
		$output = $this->request(
			'PUT', 'usersUpdate/999',
			 json_encode($data)
		);

		$this->assertEquals('{"status":false,"msg":"User not found"}', $output);
	}

	public function test_users_update_validate()
	{
		$this->request->setHeader('Content-Type', 'application/json');
		$data = [
			"address"=> "mohali" 
		];
		$output = $this->request(
			'PUT', 'usersUpdate/1',
			 json_encode($data)
		);

		$this->assertEquals('{"status":false,"errors":{"name":"The Name field is required."}}', $output);
	}

	/**
	 * Delete User
	*/ 

	public function test_users_delete()
	{
		$this->request->setHeader('Accept', 'application/json');
		$output = $this->request(
			'DELETE', 'usersDelete/1'
		);
		$this->tableupdate(['status'=>1,"created_at"=>"2021-01-11 18:02:42"],'users',1);
		$this->assertEquals('{"status":true,"msg":"User delete successfully."}', $output);
	}

	public function test_users_delete_404()
	{
		$this->request->setHeader('Accept', 'application/json');
		$output = $this->request(
			'DELETE', 'usersDelete/999'
		);
		$this->assertEquals('{"status":false,"msg":"User not found"}', $output);
	}


	/**
	 * User login
	*/ 

	public function test_users_login()
	{
		$this->request->setHeader('Accept', 'application/json');
		$data = [
			"email"=> "vipan.kumar1@myoperator.co", 
			"password"=> "123456",
		];
		$output = $this->request(
			'POST', 'login',
			 $data
		);
		unset($data['password']);
		$response = [
			"status"=> true,
			"msg"=> "login successfully.",
			"data"=> [
				"id"=> "1",
				"name"=> "vipan",
				"role_id"=> "1",
				"email"=> "vipan.kumar1@myoperator.co"
			]
			];

		$this->assertEquals(json_encode($response), $output);
	}

	public function test_users_login_validate()
	{
		$this->request->setHeader('Accept', 'application/json');
		$data = [
			"password"=> "123456",
		];
		$output = $this->request(
			'POST', 'login',
			 $data
		);
		unset($data['password']);
		$response = [
			"status"=> false,
			"errors"=> [
				"email"=> "The Email field is required."
			]
			];

		$this->assertEquals(json_encode($response), $output);
	}

	public function test_users_login_404()
	{
		$this->request->setHeader('Accept', 'application/json');
		$data = [
			"email"=> "vipan.kumar1@error.co", 
			"password"=> "123456",
		];
		$output = $this->request(
			'POST', 'login',
			 $data
		);
		unset($data['password']);
		$response = [
			"status"=> false,
			"msg"=> "These credentials do not match our records."
			];

		$this->assertEquals(json_encode($response), $output);
	}


	/**
	 * User Roles
	*/ 

	public function test_users_roles()
	{
		$output = $this->request(
			'get', 'rolesGet'
		);
		$response = [
			"status"=> true,
			"msg"=> "roles",
			"data"=> [
				[
					"role_id"=> "1",
					"role_name"=> "Admin"
				],
				[
					"role_id"=> "2",
					"role_name"=> "Manager"
				],
				[
					"role_id"=> "3",
					"role_name"=> "User"
				]
			]
				];

		$this->assertEquals(json_encode($response), $output);
	}

	/**
	 * TearDowns
	*/ 
	protected function tearDowns($data,$table): void
	{
		$CI =& get_instance();
		$CI->load->database();
		$this->db = $CI->db;
		$this->db->delete($table, $data);
	}

	protected function tableupdate($data,$table,$id): void
	{
		$CI =& get_instance();
		$CI->load->database();
		$this->db = $CI->db;
		$this->db->where('id', $id);
		$this->db->update($table, $data);
	}
}